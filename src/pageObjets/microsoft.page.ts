import { Page, PageContext } from "@testing/wdio-page-objects";
import { expect } from "chai";

const View = {
  lblMicrosoft365: "#shellmenu_0",
  lblOffice: "#shellmenu_1",
  lblWindows: "#shellmenu_2",
  lblSurface: "#shellmenu_3",
  lblXbox: "#shellmenu_4",
  lblDeals: "#shellmenu_5",
  lblSupport: "#l1_support",
  lblWindows10: "#uhf-g-nav > ul > li:nth-child(2) > div",
  lblHowToGetWindows10: "#c-shellmenu_53",
  lblBuyWindows10Home: "#c-shellmenu_54",
  lblBuyWindows10Pro: "#c-shellmenu_55",
  lblRemoteResources: "#c-shellmenu_56",
  lblWindows7EndOfSupport: "#c-shellmenu_57",
  lblWindows10Features: "#c-shellmenu_58",
  lblSyncPhonesToComputer: "#c-shellmenu_59",
  lblWindowsSecurity: "#c-shellmenu_60",
  lblCompareWindows10Editions: "#c-shellmenu_61",
  lblEspecificationsAndRequirements: "#c-shellmenu_62",
  getElementsDropdown: "#uhf-g-nav > ul > li:nth-child(2) > div > ul",
  btnSearch: "#search",
  inputSearchBar: "#cli_shellHeaderSearchInput",
  firstElement: "#universal-header-search-auto-suggest-ul > li:nth-child(1)",
  secondElement: "#universal-header-search-auto-suggest-ul > li:nth-child(2)",
  thirdElement: "#universal-header-search-auto-suggest-ul > li:nth-child(3)",
  btnGoMexicoSpanish: "#R1MarketRedirect-submit",
  lblsuscriptionPrice: "#ProductPrice_productPrice_PriceContainer",
  closeModal: "#R1MarketRedirect-1 > button",
  lblReadyForTomorrow: "#AreaHeading22_Overview",
  lblPricePageBar: "#ProductPrice_productPricePageBar_PriceContainer",
  btnAddToCart: "#buttonsPageBar_AddToCartButton",
  firstAmount:"#store-cart-root > div > div > div > section._3a6I5TkT > div > div > div > div > div > div.item-details > div.item-quantity-price > div.item-price > div > span",
  secondAmount:"#store-cart-root > div > div > div > section._3LWrsBIG > div > div > div:nth-child(2) > div > span:nth-child(1) > span:nth-child(2) > div > span",
  thirdAmout:"#store-cart-root > div > div > div > section._3LWrsBIG > div > div > div:nth-child(4) > div > span > span:nth-child(2) > strong > span",
  optionTwenty:"#store-cart-root > div > div > div > section._3a6I5TkT > div > div > div > div > div > div.item-details > div.item-quantity-price > div.item-quantity > select > option:nth-child(20)",
};
export class MicrosoftPage {
  validateTotalAmountUnitPrice() {
    $(View.optionTwenty).click();
    var firstPrice = $(View.firstAmount).getText().substring(1);
    var cantidad = parseInt(firstPrice);
    var result = cantidad * 20;
    browser.pause(3000);
    var thirdPrice = $(View.thirdAmout).getText().substring(1);
    console.log("####@"+thirdPrice);
    expect(thirdPrice).to.equals("15,980.00");
  }
  validateAmounts() {
    let firstPrice = $(View.firstAmount).getText().substring(1);
    let SecondPrice = $(View.secondAmount).getText().substring(1);
    let thirdPrice = $(View.thirdAmout).getText().substring(1);
    expect(firstPrice).to.contains(SecondPrice);
    expect(SecondPrice).to.contains(thirdPrice);
  }
  getPriceSuscription(option: String) {
    $(
      "#universal-header-search-auto-suggest-ul > li:nth-child" + option
    ).click();
    $(View.closeModal).click();
    let price = $(View.lblsuscriptionPrice).getText();
    console.log("Price:" + price);
  }
  validateLblPrice() {
    let price = $(View.lblsuscriptionPrice).getText();
    var amount1 = price.substring(1);
    const elem = $(View.lblReadyForTomorrow);
    elem.scrollIntoView();
    expect(amount1).to.equal(amount1);
  }

  goAddToCart() {
    $(View.btnAddToCart).click();
  }

  validateHeader() {
    $(View.lblMicrosoft365).isDisplayed();
    $(View.lblOffice).isDisplayed();
    $(View.lblWindows).isDisplayed();
    $(View.lblSurface).isDisplayed();
    $(View.lblXbox).isDisplayed();
    $(View.lblDeals).isDisplayed();
    $(View.lblSupport).isDisplayed();
  }
  goToWindowsOption() {
    $(View.lblWindows).click();
  }
  goToWindows10option() {
    $(View.lblWindows10).click();
  }
  getTextDropDown() {
    let data = $("#uhf-g-nav > ul > li:nth-child(2) > div > ul").getText();
    return data;
  }
  searchVisualStudio() {
    $(View.btnSearch).click();
    $(View.inputSearchBar).setValue("Visual Studio");
  }
}
