import {Given, When} from "@testing/cucumber-runner";
import {Then} from "@testing/cucumber-runner";
import {expect} from 'chai'
import { MicrosoftPage } from "../pageObjets/microsoft.page";


export class Microsoft {

    @Given(/^I visit the microsoft page$/)
    I_visit_the_microsoft_page() {
        browser.url('en-us/');
    }

    @When(/^I validate all menu items are present$/)
    I_validate_all_menu_items_are_present() {
       let microsoft = new MicrosoftPage();
        microsoft.validateHeader();
    }

    @Then(/^I go to windows option and select windows10 option$/)
    I_go_to_windows_option_and_select_windows10_option() {
        let microsoft = new MicrosoftPage();
        microsoft.goToWindowsOption();
        microsoft.goToWindows10option();
        browser.pause(4000);
        microsoft.getTextDropDown();
    }

    @Then(/^I Print all Elements in the dropdown$/)
    I_Print_all_Elements_in_the_dropdown() {
        let microsoft = new MicrosoftPage();
        console.log(microsoft.getTextDropDown());
    }

    @When(/^I search visual studio and get the first three subscriptions price$/)
    I_search_visual_studio_and_get_the_first_three_subscriptions_price() {
        let microsoft = new MicrosoftPage();
        microsoft.searchVisualStudio();
        microsoft.getPriceSuscription("(1)");
        microsoft.searchVisualStudio();
        microsoft.getPriceSuscription("(2)");
        microsoft.searchVisualStudio();
        microsoft.getPriceSuscription("(3)");
    }
    

    @When(/^I validate that the two price tags match and add to cart$/)
    I_validate_that_the_two_price_tags_match() {
        let microsoft = new MicrosoftPage();
        microsoft.searchVisualStudio();
        microsoft.getPriceSuscription("(1)");
        microsoft.validateLblPrice();
        microsoft.goAddToCart();
    }

    @When(/^I verify all 3 price amounts are the same$/)
    i_send_the_contact_us_form() {
        let microsoft = new MicrosoftPage();
        microsoft.validateAmounts();
    }

    @Then(/^I select 20 and validate the Total amount is Unit Price$/)
    I_select_20_and_validate_the_Total_amount_is_Unit_Price() {
        let microsoft = new MicrosoftPage();
        microsoft.validateAmounts();
        microsoft.validateTotalAmountUnitPrice();
    }


}