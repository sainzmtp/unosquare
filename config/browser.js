const {configBuild} = require("@testing/wdio-config");
const {config} = require("./base.conf");

const chrome = exports.chrome = {browserName: 'chrome'};
const firefox = exports.firefox = {browserName: 'firefox'};
const edge = exports.edge = {browserName: 'MicrosoftEdge'};

/**
 * https://bugs.webkit.org/show_bug.cgi?id=202589
 *
const safari = exports.safari = {
    browserName: 'safari',
    "sauce:options": {}
};
*/

// const all = exports.all = [chrome, firefox, edge];
const all = exports.all = [chrome];

exports.config = configBuild(config, {
    capabilities: all
});
